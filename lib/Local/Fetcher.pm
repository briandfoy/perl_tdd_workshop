use v5.20;

package Local::Fetcher;

use feature qw(signatures);
no warnings qw(experimental::signatures);

our $VERSION = '0.001';

use Mojo::UserAgent;
my $ua;

__PACKAGE__->set_ua( Mojo::UserAgent->new() );

sub ua ( $self ) {
	$ua;
	}

sub set_ua ( $self, $new_ua ) {
	$ua = $new_ua;
	}

sub get_webpage ( $self, $url ) {
	my $body = $self->ua->get( $url )->res->body;
	return $body;
	}

1;
__END__
=head1 NAME

Local::Fetcher - Perl extension for blah blah blah

=head1 SYNOPSIS

  use Local::Fetcher;

  my $body = Local::Fetcher->get_webpage( $url )

=head1 DESCRIPTION

Stub documentation for Local::Fetcher, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.

=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

brian d foy, E<lt>brian@localE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2016 by brian d foy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.20.0 or,
at your option, any later version of Perl 5 you may have available.

=cut
