use strict;
use warnings;

use Test::More;

use_ok( 'Local::Fetcher' ) or BAIL_OUT();

my @values = ( -7, 37, 0 );

BEGIN {
	*CORE::GLOBAL::localtime = sub { @values };

	}

subtest setup => sub {
	my @vals = localtime;
	is_deeply( \@vals, [ -7, 37, 0 ], 'Mock localtime returns expected values' ); 
	};

subtest early_morning => sub {
	@values = ( 0, 0, 7 );
	ok( (localtime)[2] < 8, "It's before 8 am" );
	};

subtest late_afternoon => sub {
	@values = ( 0, 0, 25 );
	ok( (localtime)[2] > 16, "Go home" );
	};

done_testing();
