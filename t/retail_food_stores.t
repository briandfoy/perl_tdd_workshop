
use strict;
use warnings;

use Test::More;
use Mojo::JSON qw(decode_json);

require 't/lib/utils.pl';

my $class  = 'Local::Fetcher';
my $method = 'get_webpage';

subtest setup => sub {
	use_ok( $class ) or BAIL_OUT( "$class did not compile" );
	can_ok( $class, $method );
	};

subtest retail_food_stores => sub {
	my $url = 'https://data.ny.gov/api/views/c2hv-vmqn/rows.json?accessType=DOWNLOAD';
	check_a_url( $url, $class, $method );
	};

done_testing();
