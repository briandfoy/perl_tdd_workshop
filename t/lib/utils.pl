sub check_a_url {
	my $url = shift;
	my $class = shift;
	my $method = shift;
	my $body = $class->$method( $url );
	ok( $body, "Fetched $url" );

	my $perl = eval { decode_json( $body ) } or do {
		diag( "Using fallback for $url" );
		fail( "Fetching $url" );
		};
	isa_ok( $perl, ref {} );
	ok( exists $perl->{meta}, "Has meta tag" );
	}

1;
